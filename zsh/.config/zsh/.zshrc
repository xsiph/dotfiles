# Exports
export EDITOR='lvim'
export BROWSER='firefox'
export MANPAGER='lvim +Man!'
export PATH=~/.local/bin:$PATH
export TERM="xterm-256color"
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# Source
source ~/.zsh_aliases
source ~/.profile

# Prompt
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '%b '
setopt PROMPT_SUBST
PROMPT='%B%F{green}%n%f@%F{yellow}%m%f %F{blue}%~%b%f %F{red}${vcs_info_msg_0_}%f$ '

# Alias
alias glances='glances --enable-plugin sensors --disable-plugin diskio'
alias tree='tree -aI .git '
alias git-graph='git log --graph --oneline'
# alias ls='ls --color=auto'
alias grep='grep --color=auto'

# Misc
autoload -Uz compinit && compinit
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
bindkey -v
# Colors
autoload -Uz colors && colors


-----------------------------------------------------------
-- Neovim LSP configuration file
-----------------------------------------------------------

-- This file can be loaded by calling `require('module_name')` from your
--- `init.lua`

-- plugin: nvim-lspconfig
--- For language server setup see: https://github.com/neovim/nvim-lspconfig

-- Rust --> rust-analyzer
--- https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_anaylzer
require('lspconfig').rust_analyzer.setup{}

-- Rust --> rust-tools
--- https://github.com/simrat39/rust-tools.nvim
-- require('rust-tools').setup({})

-- Typescript --> typescript-language-server
--- https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#tsserver
require('lspconfig').tsserver.setup{}

-- HTML, CSS, JavaScript --> vscode-html-languageserver
--- https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#html
--- Enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

require('lspconfig').html.setup {
    capabilities = capabilities
}

